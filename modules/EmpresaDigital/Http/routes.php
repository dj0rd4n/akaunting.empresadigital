<?php
Route::group(
	[
		'middleware' => [
			'web',
			'auth',
		],
		'prefix' => 'empresadigital',
		'namespace' => 'Modules\EmpresaDigital\Http\Controllers',
	],
	function() {
		Route::group(
			[
				'middleware' => [
					'adminmenu',
					'permission:read-admin-panel',
				],
			],
			function () {
				Route::get('/', 'EmpresaDigitalController@index');
				Route::get('/invoices/repeat/{invoice}', 'InvoicesController@repeat');
				Route::patch('/invoices/repeatSave/{invoice}', 'InvoicesController@repeatSave');
			}
		);
	}
);
