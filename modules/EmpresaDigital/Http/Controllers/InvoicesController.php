<?php

namespace Modules\EmpresaDigital\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Income\Invoice;
use App\Models\Income\InvoiceHistory;
use App\Traits\Incomes;

class InvoicesController extends Controller
{
	use Incomes;
	
	 /**
     * Show the form for payments a resource.
     * @return Response
     */
	public function view(Invoice $invoice)
    {
        $accounts = Account::enabled()->orderBy('name')->pluck('name', 'id');

        $currencies = Currency::enabled()->orderBy('name')->pluck('name', 'code')->toArray();

        $account_currency_code = Account::where('id', setting('general.default_account'))->pluck('currency_code')->first();

        $customers = Customer::enabled()->orderBy('name')->pluck('name', 'id');

        $categories = Category::enabled()->type('income')->orderBy('name')->pluck('name', 'id');

        $payment_methods = Modules::getPaymentMethods();

        return view('customers.invoices.show', compact('invoice', 'accounts', 'currencies', 'account_currency_code', 'customers', 'categories', 'payment_methods'));
    }
	
    /**
     * Show the form for repeating a resource.
     * @return Response
     */
    public function repeat(Invoice $invoice)
    {
		$recurring_frequencies = [
            'day' => trans('recurring.daily'),
            'week' => trans('recurring.weekly'),
            'month' => trans('recurring.monthly'),
            'year' => trans('recurring.yearly'),
        ];

		return view('empresadigital::invoices/repeat', compact(
			'invoice',
			'recurring_frequencies'
		));
    }

	/**
     * RepeatSave the specified resource in storage.
     *
     * @param  Invoice  $invoice
     * @param  Request  $request
     *
     * @return Response
     */
    public function repeatSave(Invoice $invoice, Request $request)
    {
		$count = $request->input('recurring_count');
		$frequency = $request->input('recurring_frequency');
		$invoiced = $request->input('invoiced_at');
		$due = $request->input('due_at');
		
		for($i = 0; $i < $count; $i++) {
			if($i) {
				$invoiced = date('Y-m-d', strtotime("$invoiced +1 $frequency"));
				$due = date('Y-m-d', strtotime("$due +1 $frequency"));
			}
			$clone = $invoice->duplicate();
			$clone->invoice_status_code = 'sent';
			$clone->invoiced_at = $invoiced;
			$clone->due_at = $due;
			$clone->save();

			// Add invoice history
			InvoiceHistory::create([
				'company_id' => session('company_id'),
				'invoice_id' => $clone->id,
				'status_code' => 'draft',
				'notify' => 0,
				'description' => trans('messages.success.added', ['type' => $clone->invoice_number]),
			]);
			InvoiceHistory::create([
				'company_id' => session('company_id'),
				'invoice_id' => $clone->id,
				'status_code' => 'sent',
				'notify' => 0,
				'description' => 'Marcar Como Enviada',
			]);

			// Update next invoice number
			$this->increaseNextInvoiceNumber();
		}

        flash('Fatura repetida com sucesso!')->success();

        return redirect('incomes/invoices');
    }
}
