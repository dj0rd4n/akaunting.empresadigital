<?php

namespace Modules\EmpresaDigital\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use View;

class EmpresaDigitalServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
		
		 // Suggestions
        View::composer(
            ['partials.admin.content'], 'App\Http\ViewComposers\Suggestions'
        );

        // Notifications
        View::composer(
            ['partials.admin.content'], 'App\Http\ViewComposers\Notifications'
        );

        // Add company info to menu
        View::composer(
            ['partials.admin.menu', 'partials.customer.menu'], 'App\Http\ViewComposers\Menu'
        );
		
		// Add Invoice Text
        View::composer(
            ['*invoices.repeat*'], 'App\Http\ViewComposers\InvoiceText'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('empresadigital.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'empresadigital'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/empresadigital');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/empresadigital';
        }, \Config::get('view.paths')), [$sourcePath]), 'empresadigital');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/empresadigital');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'empresadigital');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'empresadigital');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
