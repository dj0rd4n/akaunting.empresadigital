@extends('layouts.admin')
@section('title', 'Fatura')

@section('content')
 @stack('status_message_start')
    @if ($invoice->status->code == 'draft')
    <div class="callout callout-warning">
        @stack('status_message_body_start')
        <p>{!! trans('invoices.messages.draft') !!}</p>
        @stack('status_message_body_end')
    </div>
    @endif
    @stack('status_message_end')

    @stack('invoice_start')
    <div class="box box-success">
        <section class="invoice">
            @stack('invoice_badge_start')
            <div id="badge">
                <div class="arrow-up"></div>
                <div class="label {{ $invoice->status->label }}">{{ trans('invoices.status.' . $invoice->status->code) }}</div>
                <div class="arrow-right"></div>
            </div>
            @stack('invoice_badge_end')

            @stack('invoice_header_start')
            <div class="row invoice-header">
                <div class="col-xs-7">
                    @if (setting('general.invoice_logo'))
                        <img src="{{ Storage::url(setting('general.invoice_logo')) }}" class="invoice-logo" />
                    @elseif (setting('general.company_logo'))
                        <img src="{{ Storage::url(setting('general.company_logo')) }}" class="invoice-logo" />
                    @else
                        <img src="{{ asset('public/img/company.png') }}" class="invoice-logo" />
                    @endif
                </div>

                <div class="col-xs-5 invoice-company">
                    <address>
                        <strong>{{ setting('general.company_name') }}</strong><br>
                        {!! nl2br(setting('general.company_address')) !!}<br>
                        @if (setting('general.company_tax_number'))
                        {{ trans('general.tax_number') }}: {{ setting('general.company_tax_number') }}<br>
                        @endif
                        <br>
                        @if (setting('general.company_phone'))
                        {{ setting('general.company_phone') }}<br>
                        @endif
                        {{ setting('general.company_email') }}
                    </address>
                </div>
            </div>
            @stack('invoice_header_end')

            @stack('invoice_information_start')
            <div class="row">
                <div class="col-xs-7">
                    {{ trans('invoices.bill_to') }}
                    <address>
                        @stack('name_input_start')
                        <strong>{{ $invoice->customer_name }}</strong><br>
                        @stack('name_input_end')

                        @stack('address_input_start')
                        {!! nl2br($invoice->customer_address) !!}<br>
                        @stack('address_input_end')

                        @stack('tax_number_input_start')
                        @if ($invoice->customer_tax_number)
                        {{ trans('general.tax_number') }}: {{ $invoice->customer_tax_number }}<br>
                        @endif
                        @stack('tax_number_input_end')
                        <br>
                        @stack('phone_input_start')
                        @if ($invoice->customer_phone)
                        {{ $invoice->customer_phone }}<br>
                        @endif
                        @stack('phone_input_end')

                        @stack('email_start')
                        {{ $invoice->customer_email }}
                        @stack('email_input_end')
                    </address>
                </div>

                <div class="col-xs-5">
                    <div class="table-responsive">
                        <table class="table no-border">
                            <tbody>
                                @stack('invoice_number_input_start')
                                <tr>
                                    <th>{{ trans('invoices.invoice_number') }}:</th>
                                    <td class="text-right">{{ $invoice->invoice_number }}</td>
                                </tr>
                                @stack('invoice_number_input_end')

                                @stack('order_number_input_start')
                                @if ($invoice->order_number)
                                <tr>
                                    <th>{{ trans('invoices.order_number') }}:</th>
                                    <td class="text-right">{{ $invoice->order_number }}</td>
                                </tr>
                                @endif
                                @stack('order_number_input_end')

                                @stack('invoiced_at_input_start')
                                <tr>
                                    <th>{{ trans('invoices.invoice_date') }}:</th>
                                    <td class="text-right">{{ Date::parse($invoice->invoiced_at)->format($date_format) }}</td>
                                </tr>
                                @stack('invoiced_at_input_end')

                                @stack('due_at_input_start')
                                <tr>
                                    <th>{{ trans('invoices.payment_due') }}:</th>
                                    <td class="text-right">{{ Date::parse($invoice->due_at)->format($date_format) }}</td>
                                </tr>
                                @stack('due_at_input_end')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @stack('invoice_information_end')

            @stack('invoice_item_start')
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                @stack('actions_th_start')
                                @stack('actions_th_end')

                                @stack('name_th_start')
                                <th>{{ trans_choice($text_override['items'], 2) }}</th>
                                @stack('name_th_end')

                                @stack('quantity_th_start')
                                <th class="text-center">{{ trans($text_override['quantity']) }}</th>
                                @stack('quantity_th_end')

                                @stack('price_th_start')
                                <th class="text-right">{{ trans($text_override['price']) }}</th>
                                @stack('price_th_end')

                                @stack('taxes_th_start')
                                @stack('taxes_th_end')

                                @stack('total_th_start')
                                <th class="text-right">{{ trans('invoices.total') }}</th>
                                @stack('total_th_end')
                            </tr>
                            @foreach($invoice->items as $item)
                            <tr>
                                @stack('actions_td_start')
                                @stack('actions_td_end')

                                @stack('name_td_start')
                                <td>
                                    {{ $item->name }}
                                    @if ($item->sku)
                                        <br><small>{{ trans('items.sku') }}: {{ $item->sku }}</small>
                                    @endif
                                </td>
                                @stack('name_td_end')

                                @stack('quantity_td_start')
                                <td class="text-center">{{ $item->quantity }}</td>
                                @stack('quantity_td_end')

                                @stack('price_td_start')
                                <td class="text-right">@money($item->price, $invoice->currency_code, true)</td>
                                @stack('price_td_end')

                                @stack('taxes_td_start')
                                @stack('taxes_td_end')

                                @stack('total_td_start')
                                <td class="text-right">@money($item->total, $invoice->currency_code, true)</td>
                                @stack('total_td_end')
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @stack('invoice_item_end')

            @stack('invoice_total_start')
            <div class="row">
                <div class="col-xs-7">
                @stack('notes_input_start')
                @if ($invoice->notes)
                    <p class="lead">{{ trans_choice('general.notes', 2) }}</p>

                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        {{ $invoice->notes }}
                    </p>
                @endif
                @stack('notes_input_end')
                </div>

                <div class="col-xs-5">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                @foreach ($invoice->totals as $total)
                                @if ($total->code != 'total')
                                    @stack($total->code . '_td_start')
                                    <tr>
                                        <th>{{ trans($total->title) }}:</th>
                                        <td class="text-right">@money($total->amount, $invoice->currency_code, true)</td>
                                    </tr>
                                    @stack($total->code . '_td_end')
                                @else
                                    @if ($invoice->paid)
                                        <tr class="text-success">
                                            <th>{{ trans('invoices.paid') }}:</th>
                                            <td class="text-right">- @money($invoice->paid, $invoice->currency_code, true)</td>
                                        </tr>
                                    @endif
                                    @stack('grand_total_td_start')
                                    <tr>
                                        <th>{{ trans($total->name) }}:</th>
                                        <td class="text-right">@money($total->amount - $invoice->paid, $invoice->currency_code, true)</td>
                                    </tr>
                                    @stack('grand_total_td_end')
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @stack('invoice_total_end')

            @stack('box_footer_start')
            <div class="box-footer row no-print">
                <div class="col-md-12">
                    @stack('button_edit_start')
                    @if(!$invoice->reconciled)
                    <a href="{{ url('incomes/invoices/' . $invoice->id . '/edit') }}" class="btn btn-default">
                        <i class="fa fa-pencil-square-o"></i>&nbsp; {{ trans('general.edit') }}
                    </a>
                    @endif
                    @stack('button_edit_end')
                </div>
            </div>
            @stack('box_footer_end')
        </section>
    </div>
    @stack('invoice_end')
	
	
	<h3>Repetir Fatura</h3>
    <!-- Default box -->
    <div class="box box-success">
        {!! Form::model($invoice, ['method' => 'PATCH', 'files' => true, 'url' => ['empresadigital/invoices/repeatSave', $invoice->id], 'role' => 'form', 'class' => 'form-loading-button']) !!}

        <div class="box-body">
            {{ Form::textGroup('invoiced_at', trans('invoices.invoice_date'), 'calendar', ['id' => 'invoiced_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy-mm-dd\'', 'data-mask' => '', 'autocomplete' => 'off'], Date::parse($invoice->invoiced_at)->toDateString()) }}

            {{ Form::textGroup('due_at', trans('invoices.due_date'), 'calendar', ['id' => 'due_at', 'class' => 'form-control', 'required' => 'required', 'data-inputmask' => '\'alias\': \'yyyy-mm-dd\'', 'data-mask' => '', 'autocomplete' => 'off'], Date::parse($invoice->due_at)->toDateString()) }}


            <div class="col-md-6 input-group-recurring">
				<div class="form-group col-md-12 {{ $errors->has('recurring_frequency') ? 'has-error' : ''}}">
					{!! Form::label('recurring_frequency', trans('recurring.recurring'), ['class' => 'control-label']) !!}
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-refresh"></i></div>
						{!! Form::select('recurring_frequency', $recurring_frequencies, 'month', ['class' => 'form-control']) !!}
					</div>
					{!! $errors->first('recurring_frequency', '<p class="help-block">:message</p>') !!}
				</div>

				<div class="form-group col-md-2 hidden {{ $errors->has('recurring_count') ? 'has-error' : '' }}">
					{!! Form::label('recurring_count', trans('recurring.times'), ['class' => 'control-label']) !!}
					{!! Form::number('recurring_count', 3, ['class' => 'form-control']) !!}
					{!! $errors->first('recurring_count', '<p class="help-block">:message</p>') !!}
				</div>
			</div>
        </div>
        <!-- /.box-body -->

        @permission('update-incomes-invoices')
        <div class="box-footer">
            {{ Form::saveButtons('incomes/invoices') }}
        </div>
        <!-- /.box-footer -->
        @endpermission
        {!! Form::close() !!}
    </div>
@endsection

@push('js')
    <script src="{{ asset('vendor/almasaeed2010/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    @if (language()->getShortCode() != 'en')
    <script src="{{ asset('vendor/almasaeed2010/adminlte/plugins/datepicker/locales/bootstrap-datepicker.' . language()->getShortCode() . '.js') }}"></script>
    @endif
    <script src="{{ asset('public/js/bootstrap-fancyfile.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/almasaeed2010/adminlte/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-fancyfile.css') }}">
@endpush

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //Date picker
            $('#invoiced_at').datepicker({
                format: 'yyyy-mm-dd',
                todayBtn: 'linked',
                weekStart: 1,
                autoclose: true,
                language: '{{ language()->getShortCode() }}'
            });

            //Date picker
            $('#due_at').datepicker({
                format: 'yyyy-mm-dd',
                todayBtn: 'linked',
                weekStart: 1,
                autoclose: true,
                language: '{{ language()->getShortCode() }}'
            });
        });
    </script>
@endpush
