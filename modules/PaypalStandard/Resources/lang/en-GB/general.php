<?php

return [

    'title' => 'Paypal Standard',
    'paypalstandard' => 'Paypal Standard',

    'form' => [
        'email' => 'Email',
        'mode' => 'Mode',
        'debug' => 'Debug',
        'transaction' => 'Transaction',
        'customer' => 'Show to Customer',
        'order' => 'Order',
    ],

    'test_mode' => 'Warning: The payment gateway is in \'Sandbox Mode\'. Your account will not be charged.',
    'description' => 'Pague com cartão de crédito via PAYPAL',
    'confirm' => 'Confirm',

];
