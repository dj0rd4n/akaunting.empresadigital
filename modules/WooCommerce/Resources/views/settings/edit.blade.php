@extends('layouts.admin')

@section('title', trans_choice('general.settings', 2))

@section('content')
        {!! Form::open(['url' => 'woocommerce/settings', 'files' => true, 'role' => 'form']) !!}

            <div class="box box-success">
                <div class="box-body">
                    {{ Form::textGroup('url', trans('woocommerce::general.url'), 'globe', ['required' => 'required'], old('url', setting('woocommerce.url'))) }}

                    {{ Form::selectGroup('invoice_category_id', trans_choice('general.categories', 1), 'folder-open-o', $income_categories, old('order_category', setting('woocommerce.invoice_category_id'))) }}
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="form-group no-margin">
                            {!! Form::button('<span class="fa fa-save"></span> &nbsp;' . trans('general.save'), ['type' => 'submit', 'class' => 'btn btn-success  button-submit', 'data-loading-text' => trans('general.loading')]) !!}
                            <a href="{{ url('/') }}" class="btn btn-default"><span class="fa fa-times-circle"></span> &nbsp;{{ trans('general.cancel') }}</a>
                        </div>
                    </div>
                </div>
            </div>

        {!! Form::close() !!}
@endsection

@push('scripts')
<script type="text/javascript">

    $(document).ready(function(){
        $("#invoice_category_id").select2({
            placeholder: "{{ trans('general.form.select.field', ['field' => trans_choice('general.categories', 1)]) }}"
        });
    });
</script>
@endpush
