<?php

namespace Modules\WooCommerce\Listeners;

use App\Events\AdminMenuCreated as Event;

class AdminMenuCreated
{
    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $user = auth()->user();

        // Settings
        if ($user->can(['read-settings-settings', 'read-settings-categories', 'read-settings-currencies', 'read-settings-taxes'])) {
            // Add child to existing item
            $item = $event->menu->whereTitle(trans_choice('general.settings', 2));

            $item->url('woocommerce/settings', trans('woocommerce::general.title'), 5, ['icon' => 'fa fa-angle-double-right']);
        }
    }
}