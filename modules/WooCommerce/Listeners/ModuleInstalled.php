<?php

namespace Modules\WooCommerce\Listeners;

use App\Events\ModuleInstalled as Event;
use App\Models\Setting\Category;

class ModuleInstalled
{
    public $company_id;
    
    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        if ($event->alias != 'woocommerce') {
            return;
        }

        // Save settings
        $this->saveSettings();
    }

    protected function saveSettings()
    {
        setting()->forgetAll();
        setting()->setExtraColumns(['company_id' => session('company_id')]);
        setting()->load(true);

        $category_id = Category::enabled()->type('income')->pluck('id')->first();

        setting()->set('woocommerce.invoice_category_id', $category_id);
        setting()->save();

        setting()->forgetAll();
    }
}
