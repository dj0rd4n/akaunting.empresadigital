<?php

namespace Modules\WooCommerce\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\WooCommerce\Listeners\AdminMenuCreated;
use Modules\WooCommerce\Listeners\ModuleInstalled;

class Event extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['events']->listen(\App\Events\AdminMenuCreated::class, AdminMenuCreated::class);
        $this->app['events']->listen(\App\Events\ModuleInstalled::class, ModuleInstalled::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
