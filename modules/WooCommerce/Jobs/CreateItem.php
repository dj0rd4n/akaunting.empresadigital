<?php

namespace Modules\WooCommerce\Jobs;

use App\Models\Common\Item;
use League\Fractal;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\WooCommerce\Transformers\Product as ProductTransformer;

class CreateItem
{
    use Dispatchable;

    protected $input;

    /**
     * Create a new job instance.
     *
     * @param  $input
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function handle()
    {
        if (empty($this->input['variations'])) {
            $this->input['sku'] = !empty($this->input['sku']) ? $this->input['sku'] : 'woo-product-' . $this->input['id'];
            $item = $this->getOrCreateItem($this->input);
        } else {
            foreach ($this->input['variations'] as $variation) {
                $name = $this->input['name'];

                foreach ($variation['attributes'] as $attribute_name) {
                    $name .= ' - ' . $attribute_name;
                }

                $item = $this->getOrCreateItem([
                    'name' => $name,
                    'sku' => !empty($variation['sku']) ? $variation['sku'] : 'woo-variation-' . $variation['variation_id'],
                    'price' => $variation['display_regular_price'],
                    'quantity' => $variation['max_qty'],
                    'status' => $this->input['status'],
                ]);
            }
        }

        return $item;
    }

    protected function getOrCreateItem($input)
    {
        $array = (new Fractal\Manager())->createData(
            new Fractal\Resource\Item($input, new ProductTransformer())
        )->toArray();

        $item = Item::where('sku', $array['data']['sku'])->first();

        if (empty($item)) {
            $item = Item::create($array['data']);
        }

        return $item;
    }
}
