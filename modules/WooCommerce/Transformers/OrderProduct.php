<?php

namespace Modules\WooCommerce\Transformers;

use League\Fractal\TransformerAbstract;

class OrderProduct extends TransformerAbstract
{

    public function transform($row)
    {
        return [
            'name'                  =>  $row['name'],
            'sku'                   =>  $row['sku'],
            'price'                 =>  $row['price'],
            'quantity'              =>  $row['quantity'],
            'tax'                   =>  $row['total_tax'],
            'company_id'            =>  session('company_id'),
        ];
    }
}