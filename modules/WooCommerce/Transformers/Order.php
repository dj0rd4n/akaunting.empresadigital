<?php

namespace Modules\WooCommerce\Transformers;

use App\Models\Setting\Category;
use App\Models\Setting\Currency;
use League\Fractal\TransformerAbstract;

class Order extends TransformerAbstract
{

    public function transform($row)
    {
        $currency = Currency::enabled()->where('code', $row['currency'])->first();

        $currency_code = !empty($currency) ? $row['currency'] : setting('general.default_currency');

        return [
            'invoice_number'        =>  $row['invoice_number'],
            'order_number'          =>  $row['id'],
            'invoice_status_code'   =>  'draft',
            'invoiced_at'           =>  $row['date_created'],
            'due_at'                =>  $row['date_created'],
            'amount'                =>  $row['total'],
            'currency_code'         =>  $currency_code,
            'currency_rate'         =>  1,
            'customer_id'           =>  $row['customer_id'],
            'customer_name'         =>  $row['billing']['first_name'] . ' ' . $row['billing']['last_name'],
            'customer_email'        =>  $row['billing']['email'],
            'customer_phone'        =>  $row['billing']['phone'],
            'customer_address'      =>  $row['address'],
            //'notes'                 =>  $row['comment'],
            'category_id'           =>  $this->getCategoryId(),
            'company_id'            =>  session('company_id'),
        ];
    }

    protected function getCategoryId()
    {
        $category_id = setting('woocommerce.invoice_category_id');

        if (empty($category_id)) {
            $category_id = Category::enabled()->type('income')->pluck('id')->first();
        }

        return $category_id;
    }
}