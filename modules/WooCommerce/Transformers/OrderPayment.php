<?php

namespace Modules\WooCommerce\Transformers;

use App\Models\Setting\Currency;
use App\Utilities\Modules;
use League\Fractal\TransformerAbstract;

class OrderPayment extends TransformerAbstract
{

    public function transform($row)
    {
        $currency = Currency::enabled()->where('code', $row['currency'])->first();

        $currency_code = !empty($currency) ? $row['currency'] : setting('general.default_currency');

        $payment_method = setting('general.default_payment_method');

        // if empty payment method set first getPaymentMethods.
        if (empty($payment_method)) {
            $payment_methods = Modules::getPaymentMethods();

            foreach ($payment_methods as $method => $name) {
                $payment_method = $method;
                break;
            }
        }

        return [
            'account_id'            => setting('general.default_account'),
            'payment_method'        => $payment_method,
            'currency_code'         => $currency_code,
            'amount'                => $row['total'],
            'paid_at'               => $row['date_created'],
            'company_id'            => session('company_id'),
        ];
    }
}
