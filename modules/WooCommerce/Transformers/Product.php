<?php

namespace Modules\WooCommerce\Transformers;

use League\Fractal\TransformerAbstract;

class Product extends TransformerAbstract
{

    public function transform($row)
    {
        return [
            'name'              =>  $row['name'],
            'sku'               =>  !empty($row['sku']) ? $row['sku'] : 'woo-product-' . $row['id'],
            'sale_price'        =>  $row['price'],
            'purchase_price'    =>  $row['price'],
            'quantity'          =>  !empty($row['quantity']) ? $row['quantity'] : 1,
            'enabled'           =>  ($row['status'] == 'publish') ? 1 : 0,
            'company_id'        =>  session('company_id'),
        ];
    }
}