<?php

namespace Modules\WooCommerce\Transformers;

use League\Fractal\TransformerAbstract;

class Customer extends TransformerAbstract
{

    public function transform($row)
    {
        if (empty($row['meta']['billing_first_name'][0]) && empty($row['meta']['billing_last_name'][0])) {
            $name = $row['meta']['first_name'][0] . ' ' . $row['meta']['last_name'][0];
        } else {
            $name = $row['meta']['billing_first_name'][0] . ' ' . $row['meta']['billing_last_name'][0];
        }
        
        return [
            'name'              =>  $name,
            'email'             =>  $row['email'],
            'phone'             =>  !empty($row['meta']['billing_phone']) ? $row['meta']['billing_phone'][0] : '',
            'address'           =>  $row['address'],
            'enabled'           =>  1,
            'company_id'        =>  session('company_id'),
            'currency_code'     =>  setting('general.default_currency'),
        ];
    }
}