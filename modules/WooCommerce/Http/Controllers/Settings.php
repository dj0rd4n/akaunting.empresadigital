<?php

namespace Modules\WooCommerce\Http\Controllers;

use App\Models\Setting\Category;
use Artisan;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\WooCommerce\Http\Requests\Setting as Request;

class Settings extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        $income_categories = collect(Category::enabled()->type('income')->orderBy('name')->pluck('name', 'id'));

        return view('woocommerce::settings.edit', compact('income_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        setting()->set('woocommerce.url', $request['url']);
        setting()->set('woocommerce.invoice_category_id', $request['invoice_category_id']);
        setting()->save();

        //Artisan::call('cache:clear');

        flash(trans('messages.success.updated', ['type' => trans_choice('general.settings', 2)]))->success();

        return redirect('woocommerce/settings');
    }
}
