<?php

namespace Modules\WooCommerce\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Models\Income\Customer;
use App\Transformers\Income\Customer as Transformer;
use Dingo\Api\Routing\Helpers;
use League\Fractal;
use Modules\WooCommerce\Transformers\Customer as CustomerTransformer;

class Customers extends ApiController
{
    use Helpers;

    /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        // Add CRUD permission check
        $this->middleware('permission:create-incomes-customers')->only(['create', 'store', 'duplicate', 'import']);
        $this->middleware('permission:read-incomes-customers')->only(['index', 'show', 'edit']);
        $this->middleware('permission:update-incomes-customers')->only(['update']);
        $this->middleware('permission:delete-incomes-customers')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function index()
    {
        $customers = $this->api->get('api/customers');

        return $this->response->paginator($customers, new Transformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int|string  $id
     * @return \Dingo\Api\Http\Response
     */
    public function show($id)
    {
        $customer = $this->api->get('api/customers/' . $id);

        return $this->response->item($customer, new Transformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function store()
    {
        $array = (new Fractal\Manager())->createData(
            new Fractal\Resource\Item(request()->input(), new CustomerTransformer())
        )->toArray();
        
        $customer = Customer::where('email', $array['data']['email'])->first();

        if (empty($customer)) {
            $customer = Customer::create($array['data']);

            return $this->response->created(url('api/woocommerce/customers/' . $customer->id));
        } else {
            $customer->update($array['data']);
            
            return $this->response->item($customer->fresh(), new Transformer());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     * @return \Dingo\Api\Http\Response
     */
    public function update($id)
    {
        return $this->response->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Dingo\Api\Http\Response
     */
    public function destroy($id)
    {
        $this->api->get('api/customers/' . $id)->delete();

        return $this->response->noContent();
    }
}
