<?php

namespace Modules\WooCommerce\Http\Controllers;

use App\Models\Common\Company;
use App\Transformers\Common\Company as Transformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ApiController extends Controller
{
    use Helpers;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function show()
    {
        $id = request('company_id');

        $companies = app('Dingo\Api\Auth\Auth')->user()->companies()->pluck('id')->toArray();
        if (!in_array($id, $companies)) {
            $this->response->errorUnauthorized();
        }

        $company = Company::find($id);

        $company->setSettings();

        return $this->response->item($company, new Transformer());
    }
}
