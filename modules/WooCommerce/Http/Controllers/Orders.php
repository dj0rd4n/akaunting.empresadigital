<?php

namespace Modules\WooCommerce\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Income\Invoice as Request;
use App\Http\Requests\Income\InvoicePayment as PaymentRequest;
use App\Jobs\Income\CreateInvoice;
use App\Jobs\Income\CreateInvoicePayment;
use App\Models\Income\Invoice;
use App\Transformers\Income\Invoice as Transformer;
use Dingo\Api\Routing\Helpers;
use League\Fractal;
use Modules\WooCommerce\Transformers\Customer as CustomerTransformer;
use Modules\WooCommerce\Transformers\Order as OrderTransformer;
use Modules\WooCommerce\Transformers\OrderPayment as OrderPaymentTransformer;
use Modules\WooCommerce\Transformers\OrderProduct as OrderProductTransformer;

class Orders extends ApiController
{
    use Helpers;

    /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        // Add CRUD permission check
        $this->middleware('permission:create-incomes-invoices')->only(['create', 'store', 'duplicate', 'import']);
        $this->middleware('permission:read-incomes-invoices')->only(['index', 'show', 'edit']);
        $this->middleware('permission:update-incomes-invoices')->only(['update']);
        $this->middleware('permission:delete-incomes-invoices')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function index()
    {
        $invoices = $this->api->get('api/invoices');

        return $this->response->paginator($invoices, new Transformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Dingo\Api\Http\Response
     */
    public function show($id)
    {
        $invoice = $this->api->get('api/invoices/' . $id);

        return $this->response->item($invoice, new Transformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function store()
    {
        $is_update = false;

        $request = request();

        $array = (new Fractal\Manager())->createData(
            new Fractal\Resource\Item($request->input(), new OrderTransformer())
        )->toArray();

        $invoice_data = $array['data'];

        // Delete existing invoice
        $inv = Invoice::where('invoice_number', $invoice_data['invoice_number'])->first();
        if (!empty($inv)) {
            $this->deleteRelationships($inv, ['items', 'item_taxes', 'histories', 'payments', 'recurring', 'totals']);
            $inv->delete();

            $is_update = true;
        }

        if ($request['items']) {
            foreach ($request['items'] as $i) {
                $array = (new Fractal\Manager())->createData(
                    new Fractal\Resource\Item($i, new OrderProductTransformer())
                )->toArray();

                $invoice_data['item'][] = $array['data'];
            }
        }

        $invoice_request = new Request();
        $invoice_request->merge($invoice_data);
        $invoice_request['totals'] = !empty($request['totals']) ? $request['totals'] : [];

        $invoice = dispatch(new CreateInvoice($invoice_request));

        // Mark paid
        $invoice->invoice_status_code = 'paid';
        $invoice->save();

        $array = (new Fractal\Manager())->createData(
            new Fractal\Resource\Item($request->input(), new OrderPaymentTransformer())
        )->toArray();

        $payment_request = new PaymentRequest();
        $payment_request->merge($array['data']);
        $payment_request['invoice_id'] = $invoice->id;
        $payment_request['currency_rate'] = $invoice->currency_rate;

        $invoice_payment = dispatch(new CreateInvoicePayment($payment_request, $invoice));

        if ($is_update) {
            return $this->response->item($invoice->fresh(), new Transformer());
        }

        return $this->response->created(url('api/woocommerce/orders/' . $invoice->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     * @return \Dingo\Api\Http\Response
     */
    public function update($id)
    {
        return $this->response->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Dingo\Api\Http\Response
     */
    public function destroy($id)
    {
        $this->api->get('api/invoices/' . $id)->delete();

        return $this->response->noContent();
    }
}
