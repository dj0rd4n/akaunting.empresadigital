<?php

namespace Modules\WooCommerce\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Transformers\Common\Item as Transformer;
use Dingo\Api\Routing\Helpers;
use League\Fractal;
use Modules\WooCommerce\Jobs\CreateItem;
use Modules\WooCommerce\Transformers\Product as ProductTransformer;

class Products extends ApiController
{
    use Helpers;

    /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        // Add CRUD permission check
        $this->middleware('permission:create-common-items')->only(['create', 'store', 'duplicate', 'import']);
        $this->middleware('permission:read-common-items')->only(['index', 'show', 'edit']);
        $this->middleware('permission:update-common-items')->only(['update']);
        $this->middleware('permission:delete-common-items')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function index()
    {
        $items = $this->api->get('api/items');

        return $this->response->paginator($items, new Transformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int|string  $id
     * @return \Dingo\Api\Http\Response
     */
    public function show($id)
    {
        $item = $this->api->get('api/items/' . $id);

        return $this->response->item($item, new Transformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function store()
    {
        $item = dispatch(new CreateItem(request()->input()));

        return $this->response->created(url('api/woocommerce/products/' . $item->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     * @return \Dingo\Api\Http\Response
     */
    public function update($id)
    {
        return $this->response->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Dingo\Api\Http\Response
     */
    public function destroy($id)
    {
        $this->api->get('api/items/' . $id)->delete();

        return $this->response->noContent();
    }
}
