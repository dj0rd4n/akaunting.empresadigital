<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware' => ['api']], function($api) {
    $api->group(['prefix' => 'woocommerce', 'namespace' => 'Modules\WooCommerce\Http\Controllers'], function($api) {
        $api->get('check', 'ApiController@show');
        $api->resource('products', 'Products');
        $api->resource('customers', 'Customers');
        $api->resource('orders', 'Orders');
    });
});

Route::group(['middleware' => 'admin', 'prefix' => 'woocommerce', 'namespace' => 'Modules\WooCommerce\Http\Controllers'], function () {
    Route::get('settings', 'Settings@edit');
    Route::post('settings', 'Settings@update');
});